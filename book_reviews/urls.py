from django.contrib import admin
from django.urls import path, include, reverse_lazy
from django.views.generic.base import RedirectView

urlpatterns = [
    path('admin/', admin.site.urls),
    path("reviews/", include("reviews.urls")),
    path("", RedirectView.as_view(url=reverse_lazy("reviews_list")))
]